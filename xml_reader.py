#!usr/bin/env python
# -*-coding: utf-8 -*-

from xml.dom.minidom import parse
import xml.dom.minidom



global string_items
global string_items_cn


def xml_reader_init():
    global string_items
    global string_items_cn
    DOMTree = xml.dom.minidom.parse(".//xml_files//strings_en.xml")
    DOMTree_cn = xml.dom.minidom.parse(".//xml_files//strings_cn.xml")
    collection = DOMTree.documentElement
    collection_cn = DOMTree_cn.documentElement
    if collection.hasAttribute("xmlns:xliff"):
        print("strings_en.xml read ok. Root element : %s" % collection.getAttribute("xmlns:xliff"))

    if collection_cn.hasAttribute("xmlns:xliff"):
        print("strings_en.xml read ok. Root element : %s" % collection.getAttribute("xmlns:xliff"))

    # 获取所有的string，英文
    string_items = collection.getElementsByTagName("string")

    # 获取所有的string，中文
    string_items_cn = collection_cn.getElementsByTagName("string")


def print_string_items():
    global string_items
    print("*****"+str(string_items.length)+" strings start:*****")
    for string_item in string_items:
        if string_item.hasAttribute("name"):
            print(string_item.getAttribute("name"))
            # print("name: %s" % string_item.childNodes[0].data)
    print("*****strings end*****")


def print_string_items_value():
    global string_items
    print("*****"+str(string_items.length)+" strings value start:*****")
    for string_item in string_items:
        if string_item.hasAttribute("name"):
            # print(string_item.getAttribute("name"))
            print("name: %s" % string_item.childNodes[0].data)
    print("*****strings value end*****")


def print_string_items_value_cn():
    global string_items_cn
    print("*****"+str(string_items_cn.length)+" strings 中文 start:*****")
    for string_item in string_items_cn:
        if string_item.hasAttribute("name"):
            # print(string_item.getAttribute("name"))
            print("name: %s" % string_item.childNodes[0].data)
    print("*****strings 中文 end*****")


if __name__ == '__main__':
    xml_reader_init()
    print_string_items()
    print_string_items_value()
    print_string_items_value_cn()
