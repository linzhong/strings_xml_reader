# strings_xml_reader
# 读取Android开发APP中的strings.xml文件
---------------------------------------

## 编写目的：
    客户需要将strings.xml文件转成excel文件，便于发给相关翻译人员进行多国语言的翻译，为了避免反复几百次的复制粘贴，所以做了读取工具。
## 功能：
1. 将所有的strings item名列出；
1. 将所有的strings value值（英文）列出；
1. 将所有的strings value值（中文）列出；